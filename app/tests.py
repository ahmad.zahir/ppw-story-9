from django.test import TestCase
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from . import views
from .forms import SearchForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
# Create your tests here.
class Story9UnitTest(TestCase):
    def testIfLandingPageUrlExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIfLandingPageUsesIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def testIfLandingPageUsesIndexhtml(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testSearchBoxForm(self):
        formData = {
        'query' : 'web programming'
        }
        form = SearchForm(data=formData)
        self.assertTrue(form.is_valid())

class StoryNineFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        super().tearDown()
        self.browser.quit()

    def testIfSearchBookPageTitleContainSearchBook(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Book Search', self.browser.title)

    def testIfPageContainsSearchForm(self):
        self.browser.get(self.live_server_url)
        self.assertTrue(self.browser.find_element_by_id("search_form"))

    def testIfSearchFormContainsATextField(self):
        self.browser.get(self.live_server_url)
        form = self.browser.find_element_by_id("search_form")
        self.assertTrue(form.find_element_by_xpath("//input[@type='text']"))

    def testIfSearchFormContainsASubmitButtonWithTextSearch(self):
        self.browser.get(self.live_server_url)
        form = self.browser.find_element_by_id("search_form")
        self.assertTrue(form.find_element_by_xpath("//button[@type='submit']").text=='Search')

    def testIfSearchWithASingleWordReturnsResult(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('Web')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        table = result.find_element_by_tag_name("table")
        list_count = len(table.find_elements_by_tag_name("tr"))
        self.assertGreater(list_count, 1)

    def testIfSearchWithASentenceReturnsResult(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('Web Design using Django')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        table = result.find_element_by_tag_name("table")
        list_count = len(table.find_elements_by_tag_name("tr"))
        self.assertGreater(list_count, 1)

    def testIfSearchWithAnUnreadableTextReturnsANotFoundMessage(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('Wsncowlwnvwoirn')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        self.assertIn("No result found", result.text)

    def testIfSearchWithAnEmptyStringDoesNotChangeAnything(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        self.assertEqual("", result.text)

    def testIfAClickToLikeButtonIncrementLikeByOne(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('Web Design using Django')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        table = result.find_element_by_tag_name("table")
        books = table.find_elements_by_tag_name("tr")
        first_book = books[1]
        like_amt = first_book.find_element_by_class_name("like_amt")
        like_btn = first_book.find_element_by_xpath("//button[@class='like_btn']")
        like_btn.click()
        time.sleep(3)
        self.assertIn("1", like_amt.text)
    def testIfTwoClicskToLikeButtonIncrementLikeByTwo(self):
        self.browser.get(self.live_server_url)
        textfield = self.browser.find_element_by_id("search_form").find_element_by_xpath("//input[@type='text']")
        textfield.send_keys('Web Design using Django')
        textfield.send_keys(Keys.RETURN)
        time.sleep(3)
        result = self.browser.find_element_by_id("result")
        table = result.find_element_by_tag_name("table")
        books = table.find_elements_by_tag_name("tr")
        first_book = books[1]
        like_amt = first_book.find_element_by_class_name("like_amt")
        like_btn = first_book.find_element_by_xpath("//button[@class='like_btn']")
        like_btn.click()
        like_btn.click()
        time.sleep(3)
        self.assertIn("2", like_amt.text)
