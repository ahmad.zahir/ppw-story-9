from django.shortcuts import render
from .forms import SearchForm
# Create your views here.
def index(request):
    form = SearchForm(request.GET or None)
    return render(request, 'index.html', {'form' : form})
