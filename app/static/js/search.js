
$(document).ready(function(){
  var dict = {
    id : [],
    like : []
  };
  function getLike(num){
    if(dict.id.includes(num)){
      var idx = dict.id.indexOf(num);
      return dict.like[idx] + " likes";
    }
    else{
      return "0 likes";
    }
  }

  function addLike(num){
    if(dict.id.includes(num)){
      var idx = dict.id.indexOf(num);
      dict.like[idx] = dict.like[idx] + 1;
    }
    else{
      dict.id.push(num);
      dict.like.push(1);
      console.log("berhasil");
    }
  }

  function updateLike(num){
    $('#'+num+' .like_amt').html(getLike(num));
  }
  $('#search_form').submit(function(e){
    e.preventDefault();
    var query = $('input[name="query"]').val();
    $.ajax({
      dataType: "json",
      type: $(this).attr("method"),
      url: 'https://www.googleapis.com/books/v1/volumes?q=' + query,
      success: function(books){
        if(books.totalItems>0){
          if(books.totalItems>=10){
            $('#result').html('Showing 10 from ' + books.totalItems + ' books for query "' + query);
          }
          else{
            $('#result').html('Showing ' + books.totalItems+ ' from ' + books.totalItems + ' books for query "' + query);
          }
          $('#result').append('" <br> <table id="buku-buku" style="border-collapse: collapse;"><tr><th>No.</th><th>Thumbnail</th><th>Title</th><th>Authors</th><th>Publisher</th><th>Year</th><th>Likes</th><th>Like This Book!</th></tr></table>');
          $.each(books.items,function(i, book){
            var no = i + 1;

            if(book.volumeInfo.hasOwnProperty('publishedDate')){
              var year = book.volumeInfo.publishedDate.substring(0,4);
            }
            else{
              var year = 'undefined';
            }

            if(book.volumeInfo.hasOwnProperty('imageLinks')){
              var thumbnail = book.volumeInfo.imageLinks.thumbnail;
            }
            else{
              var thumbnail = "static/img/na.jpg";
            }
            var likes = getLike(book.id);
            $('#buku-buku').append('<tr id="'+book.id+'"><td>' + no + '</td><td><img src="' + thumbnail +'"></td><td>'+ book.volumeInfo.title + '</td><td>' + book.volumeInfo.authors + '</td><td>' + book.volumeInfo.publisher +'</td><td>' + year + '</td><td class="like_amt">'+ likes + '</td><td><button class="like_btn">♥</button></td></tr>');
            $('#'+book.id+' .like_btn').on('click', function(){addLike(book.id);updateLike(book.id);});
          });
        }
        else{
          $('#result').html('No result found for query "' + query +'"');
        }
      }
    });
  });
  $('')
});
